import Plateau from './Plateau';
import Rover from './Rover';

export default class Controller {
  constructor () {
    this.roverInfos = [];
    this.plateau = null;
  }

  printRoversLocation () {
    let res = [];
    this.roverInfos.forEach(roverInfos => {
      const rover = roverInfos.rover;
      res.push(`${rover.xPosition} ${rover.yPosition} ${rover.direction}`);
    });

    return res.join('\n');
  }

  executeCommands (Command) {
    const Commands = this.CommandSplitAndTrim(Command);

    const plateauCommands = Commands.plateauCommands;
    this.setPlateauFromCommand(plateauCommands);

    Commands.roverCommands.forEach(roverCommand => {
      this.setRoverFromCommand(roverCommand);
    });


    this.roverInfos.forEach(roverInfo => {
      this.moveRoverFromCommands(roverInfo.rover, roverInfo.moveRoverCommand, this.canMoveStrategy);
    });
  }

  validateSetPlateauCommand (Command) {
    return /^\d+\s+\d+$/.test(Command);
  }

  validateSetRoverCommand (Command) {
    return /^\d+\s\d+\s[ENSW]$/.test(Command);
  }

  validateMoveRoverCommand (Command) {
    return /^[RLM]+$/.test(Command);
  }

  CommandSplitAndTrim (Command) {
    let CommandLines = Command.trim().split('\n');
    CommandLines = CommandLines.filter(CommandLine => {
      return CommandLine.trim() !== '';
    });

    const plateauCommands = CommandLines[0];

    const roverCommandLines = CommandLines.slice(1);

    let roverCommands = [];
    for (let i = 0; i < roverCommandLines.length; i += 2) {
      roverCommands.push(
        {
          setRoverCommand: roverCommandLines[i],
          moveRoverCommand: roverCommandLines[i + 1]
        });
    }

    return {
      plateauCommands,
      roverCommands,
    }
  }

  setPlateauFromCommand (plateauCommand) {
    if (!this.validateSetPlateauCommand(plateauCommand)) {
      throw Error(`Plateau Commands is not correct: "${plateauCommand}"`);
    }
    const args = plateauCommand.split(' ').map(i => parseInt(i));

    this.plateau = new Plateau(...args);
  }

  setRoverFromCommand ({setRoverCommand, moveRoverCommand}) {
    if (!this.plateau) {
      throw Error('Plateau is not set, you can\'t set rover');
    }

    let args = setRoverCommand.split(' ');
    args = args.slice(0, 2).map(i => parseInt(i)).concat(args[2]);

    if (this.plateau.xPosition < args[0] || this.plateau.yPosition < args[1]) {
      throw Error(`Rover can't be set out of Plateau`)
    }

    if (!this.validateSetRoverCommand(setRoverCommand)) {
      throw Error(`Rover set Command is not correct: "${setRoverCommand}"`);
    }

    if (!this.validateMoveRoverCommand(moveRoverCommand)) {
      throw Error(`Rover move Command is not correct: "${moveRoverCommand}"`);
    }

    this.roverInfos.push({rover: new Rover(...args), moveRoverCommand})
  }

  canMoveStrategy (rover, rovers, plateau) {
    const maxXPosition = plateau.xPosition;
    const maxYPosition = plateau.yPosition;

    let xPosition = rover.xPosition;
    let yPosition = rover.yPosition;
    let direction = rover.direction;

    let mockRover = {xPosition, yPosition, direction};
    rover.move.apply(mockRover);
    const afterMoveStillInPlateau = mockRover.xPosition <= maxXPosition &&
      mockRover.yPosition <= maxYPosition &&
      mockRover.xPosition >= 0 &&
      mockRover.yPosition >= 0;

    let theNextPositionIsAvailable = true;
    let roverIndex = rovers.indexOf(rover);
    rovers.slice(0, roverIndex).forEach(rover => {
      if (rover.xPosition === mockRover.xPosition && rover.yPosition === mockRover.yPosition) {
        theNextPositionIsAvailable = false;
      }
    });
    return afterMoveStillInPlateau && theNextPositionIsAvailable;
  }

  moveRoverFromCommands (rover, moveRoverCommands, canMoveStrategy) {
    for (let Command of moveRoverCommands) {
      this.moveRoverFromCommand(rover, Command, canMoveStrategy);
    }
  }

  moveRoverFromCommand (rover, Command, canMoveStrategy) {
    switch (Command) {
      case 'M':
        if (canMoveStrategy) {
          if (canMoveStrategy(rover, this.roverInfos.map(roverInfo => roverInfo.rover), this.plateau)) {
            rover.move();
          }
        } else {
          rover.move();
        }
        break;
      case 'L':
        rover.turnLeft();
        break;
      case 'R':
        rover.turnRight();
        break;
      default:
        throw Error(`Unknown Rover move Command: "${Command}"`);
    }
  }
}