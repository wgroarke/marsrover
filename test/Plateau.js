import test from 'ava';
import Plateau from '../src/Plateau';

test('Plateau can be build up', t => {
  const plateau = new Plateau(10, 10);
  t.is(plateau.xPosition, 10);
  t.is(plateau.yPosition, 10);
});

test('Plateau Positions must be integer', t => {
  const error = t.throws(function () {
    new Plateau(10.1, 10.2);
  }, Error);
  t.is(error.message, 'Plateau Positions must be integers');
});

test('Plateau Positions must be integer', t => {
  const error = t.throws(function () {
    new Plateau(0, 0);
  }, Error);
  t.is(error.message, 'Plateau Positions must be positive');
});