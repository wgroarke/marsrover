const directionS = ['W', 'N', 'E', 'S'];

export default class Rover {
  constructor (xPosition, yPosition, direction) {
    if (!Number.isInteger(xPosition) || !Number.isInteger(yPosition)) {
      throw new Error('Rover Positions  integers are required');
    }

    if (xPosition < 0 || yPosition < 0) {
      throw new Error('Rover Positions must be positive');
    }

    if (directionS.indexOf(direction) < 0) {
      throw new Error('Rover direction can only be a value of "W, N, E, S"');
    }

    this.xPosition = xPosition;
    this.yPosition = yPosition;
    this.direction = direction;
  }

  move () {
    switch (this.direction) {
      case 'W': {
        this.xPosition--;
        break;
      }
      case 'N': {
        this.yPosition++;
        break;
      }
      case 'E': {
        this.xPosition++;
        break;
      }
      case 'S': {
        this.yPosition--;
        break;
      }
      default:
        break;
    }
  }

  turnLeft () {
    const directionIndex = directionS.indexOf(this.direction) - 1;
    this.direction = directionS[directionIndex < 0 ? directionIndex + 4 : directionIndex];
  }

  turnRight () {
    const directionIndex = directionS.indexOf(this.direction) + 1;
    this.direction = directionS[directionIndex > 3 ? directionIndex - 4 : directionIndex];
  }
}