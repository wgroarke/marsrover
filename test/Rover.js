import test from 'ava';
import Rover from '../src/Rover';

test('Rover can be build up', t => {
  const plateau = new Rover(10, 10, 'E');
  t.is(plateau.xPosition, 10);
  t.is(plateau.yPosition, 10);
  t.is(plateau.direction, 'E');
});

test('Rover Positions must be integer', t => {
  const error = t.throws(function () {
    new Rover(10.1, 10.2, 'E');
  }, Error);
  t.is(error.message, 'Rover Positions must be integers')
});

test('Rover Positions must be non-negative', t => {
  const error = t.throws(function () {
    new Rover(-1, 0, 'E');
  }, Error);
  t.is(error.message, 'Rover Positions must be non-negative')
});

test('Rover direction must be one of "W, N, E, S"', t => {
  const error = t.throws(function () {
    new Rover(0, 0, 'abc');
  }, Error, 'Rover direction must be one of "W, N, E, S"');
  t.is(error.message, 'Rover direction must be one of "W, N, E, S"')
});


test('Rover can move forward its direction', t => {
  const RoverN = new Rover(10, 10, 'N');
  RoverN.move();
  t.is(RoverN.xPosition, 10);
  t.is(RoverN.yPosition, 11);
  t.is(RoverN.direction, 'N');

  const RoverE = new Rover(10, 10, 'E');
  RoverE.move();
  t.is(RoverE.xPosition, 11);
  t.is(RoverE.yPosition, 10);
  t.is(RoverE.direction, 'E');


  const RoverW = new Rover(10, 10, 'W');
  RoverW.move();
  t.is(RoverW.xPosition, 9);
  t.is(RoverW.yPosition, 10);
  t.is(RoverW.direction, 'W');

  const RoverS = new Rover(10, 10, 'S');
  RoverS.move();
  t.is(RoverS.xPosition, 10);
  t.is(RoverS.yPosition, 9);
  t.is(RoverS.direction, 'S');
});

test('Rover can turn left and turn right its direction', t => {
  const RoverN = new Rover(10, 10, 'N');
  RoverN.turnRight();
  t.is(RoverN.direction, 'E');
  RoverN.turnRight();
  t.is(RoverN.direction, 'S');
  RoverN.turnRight();
  t.is(RoverN.direction, 'W');
  RoverN.turnRight();
  t.is(RoverN.direction, 'N');

  const RoverE = new Rover(10, 10, 'E');
  RoverE.turnLeft();
  t.is(RoverE.direction, 'N');
  RoverE.turnLeft();
  t.is(RoverE.direction, 'W');
  RoverE.turnLeft();
  t.is(RoverE.direction, 'S');
  RoverE.turnLeft();
  t.is(RoverE.direction, 'E');
});
