import test from 'ava';
import Controller from '../src/Controller';
import Plateau from '../src/Plateau';
import Rover from '../src/Rover';

test('Controller can split and trim input commands', t => {
  const Command = '5 5\n1 2 N\nLMLMLMLMM';

  const Command2 = '5 5\n1 2 N\n\n\nLLL\n3 4 S\nMMMM';

  const controller = new Controller();

  t.deepEqual(controller.CommandSplitAndTrim(Command),
    {plateauCommands: '5 5', roverCommands: [{setRoverCommand: '1 2 N', moveRoverCommand: 'LMLMLMLMM'}]});
  t.deepEqual(controller.CommandSplitAndTrim(Command2),
    {
      plateauCommands: '5 5',
      roverCommands: [{setRoverCommand: '1 2 N', moveRoverCommand: 'LLL'}, {
        setRoverCommand: '3 4 S',
        moveRoverCommand: 'MMMM'
      }]
    });
});

test('Controller validate Command', t => {
  const setPlateauCommand = '5 5';
  const FAIL_setPlateauCommand = 'R 5';
  const setMarsCommand = '5 5 N';
  const FAIL_setMarsCommand = '5 N 5';
  const moveMarsCommand = 'LRMMRLRML';
  const FAIL_moveMarsCommand = 'LRMMRLR3L';

  const controller = new Controller();
  t.true(controller.validateSetPlateauCommand(setPlateauCommand));
  t.false(controller.validateSetPlateauCommand(FAIL_setPlateauCommand));

  t.true(controller.validateSetRoverCommand(setMarsCommand));
  t.false(controller.validateSetRoverCommand(FAIL_setMarsCommand));

  t.true(controller.validateMoveRoverCommand(moveMarsCommand));
  t.false(controller.validateMoveRoverCommand(FAIL_moveMarsCommand));
});


test('Controller can set Plateau from Command', t => {
  const controller = new Controller();

  controller.setPlateauFromCommand('5 3');
  t.is(controller.plateau.xPosition, 5);
  t.is(controller.plateau.yPosition, 3);
});

test('Controller can set Plateau from Command, Plateau Positions must be positive', t => {
  const controller = new Controller();
  const error = t.throws(() => {
    controller.setPlateauFromCommand('0 0');
  }, Error);
  t.is(error.message, 'Plateau Positions must be positive');
});

test('Controller can set Plateau from Command, Plateau Positions should be number', t => {
  const controller = new Controller();
  const error = t.throws(() => {
    controller.setPlateauFromCommand('L 0');
  }, Error);
  t.is(error.message, 'Plateau Commands is not correct: "L 0"');
});

test('Controller can set Rover from Command', t => {
  const controller = new Controller();
  controller.plateau = new Plateau(5, 5);

  controller.setRoverFromCommand({setRoverCommand: '5 3 N', moveRoverCommand: 'MMMLR'});
  controller.setRoverFromCommand({setRoverCommand: '3 5 E', moveRoverCommand: 'MMMLR'});

  t.is(controller.roverInfos[0].rover.xPosition, 5);
  t.is(controller.roverInfos[0].rover.yPosition, 3);
  t.is(controller.roverInfos[0].rover.direction, 'N');
  t.is(controller.roverInfos[0].moveRoverCommand, 'MMMLR');

  t.is(controller.roverInfos[1].rover.xPosition, 3);
  t.is(controller.roverInfos[1].rover.yPosition, 5);
  t.is(controller.roverInfos[1].rover.direction, 'E');
  t.is(controller.roverInfos[1].moveRoverCommand, 'MMMLR');
});

test('Controller will throw error if  Rover posn set before setting plateau', t => {
  const controller = new Controller();

  const error = t.throws(() => {
    controller.setRoverFromCommand({setRoverCommand: '5 5 N', moveRoverCommand: 'MMMLR4'});
  }, Error);

  t.is(error.message, 'Plateau is not set, you may not set the Rover yet')
});

test('Controller error when setting the Rover from a  wrong setting Command', t => {
  const controller = new Controller();
  controller.plateau = new Plateau(5, 5);

  const error = t.throws(() => {
    controller.setRoverFromCommand({setRoverCommand: '5 N N', moveRoverCommand: 'MMMLR'});
  }, Error);

  t.is(error.message, 'Rover set Command is not correct: "5 N N"')
});

test('Controller will throw error when set Rover from wrong move Command', t => {
  const controller = new Controller();
  controller.plateau = new Plateau(5, 5);

  const error = t.throws(() => {
    controller.setRoverFromCommand({setRoverCommand: '5 5 N', moveRoverCommand: 'MMMLR4'});
  }, Error);

  t.is(error.message, 'Rover move Command is not correct: "MMMLR4"')
});

test('Controller can drive the Rover to move', t => {
  const controller = new Controller();
  controller.plateau = new Plateau(5, 5);

  const rover = new Rover(0, 0, 'N');
  controller.moveRoverFromCommands(rover, 'MMRMMM');
  t.is(rover.xPosition, 3);
  t.is(rover.yPosition, 2);
});

test('Controller move strategy cannot drive the Rover to move away from the plateau', t => {
  const controller = new Controller();
  const plateau = new Plateau(5, 5);

  const rover = new Rover(0, 0, 'N');
  const rover2 = new Rover(0, 0, 'S');
  const rovers = [rover, rover2];

  t.true(controller.canMoveStrategy(rover, rovers, plateau));
  t.false(controller.canMoveStrategy(rover2, rovers, plateau));
});

test('Controller move strategy cannot drive the Rover to move to the overlap of another Rover', t => {
  const controller = new Controller();
  const plateau = new Plateau(5, 5);

  const rover = new Rover(0, 1, 'N');
  const rover2 = new Rover(0, 0, 'N');
  const rover3 = new Rover(0, 3, 'N');
  const rovers = [rover, rover2, rover3];

  t.false(controller.canMoveStrategy(rover2, rovers, plateau));
  t.true(controller.canMoveStrategy(rover3, rovers, plateau));
});


test('Controller test move strategy will not actually move the Rover', t => {
  const controller = new Controller();
  const plateau = new Plateau(5, 5);

  const rover = new Rover(0, 1, 'N');
  const rover2 = new Rover(0, 0, 'N');
  const rover3 = new Rover(0, 3, 'N');
  const rovers = [rover, rover2, rover3];

  t.true(controller.canMoveStrategy(rover, rovers, plateau));
  t.is(rover.xPosition, 0);
  t.is(rover.yPosition, 1);
});

test('Controller can drive the Rover , so output the correct position', t => {
  const controller = new Controller();
  controller.executeCommands('5 5\n1 2 N\nLMLMLMLMM');
  t.is(controller.printRoversLocation(), '1 3 N');

  const controller2 = new Controller();
  controller2.executeCommands('5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM');
  t.is(controller2.printRoversLocation(), '1 3 N\n5 1 E');
});

